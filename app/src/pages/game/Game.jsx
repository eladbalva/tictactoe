import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Board from "./components/Board";

const BOARD_SIZE = 9;

function Game() {
    return (
        <div className="container">
            <Header></Header>
            <Board boardSize={BOARD_SIZE}></Board>
            <button onClick={() => {window.location.reload()}}>Start Over</button>
            <Footer></Footer>
        </div>
    );
}

export default Game;
