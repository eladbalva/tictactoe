
function Cell({handleClick, id, imgSrc, imgAlt}) {
    return (
        <div className="cell">
            <img id={id} src={imgSrc} alt={imgAlt} onClick={(e) => handleClick(e)}></img>
        </div>
    );
}

export default Cell;