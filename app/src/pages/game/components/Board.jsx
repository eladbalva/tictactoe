import { useState, useEffect } from "react";
import Cell from "./Cell";

import DEFAULT_IMG from "../../../assets/blank.jpg"
import PLAYER_1_IMG from "../../../assets/ttt-x.gif";
import PLAYER_2_IMG from "../../../assets/ttt-o.gif";

const DEFAULT_NAME = ""
const PLAYER_1_NAME = "X";
const PLAYER_2_NAME = "O";

let numBoard = [[0, 0, 0],
                [0, 0, 0], //in order to calculate if a player won.
                [0, 0, 0]];

function Board(props) {
    const [currPlayer, setCurrPlayer] = useState(true); //true means first player, false means second.
    const [turnNum, setTurnNum] = useState(1); //counts the amount of turns in the game.
    const [gameEndMessage, setGameEndMessage] = useState(""); //shows up when a player wins.

    const updateImage = ({target}) => {
        if (target.alt === DEFAULT_NAME) {
            const playerName = currPlayer ? PLAYER_1_NAME : PLAYER_2_NAME;
            target.src = currPlayer ? PLAYER_1_IMG : PLAYER_2_IMG;
            target.alt = playerName;
            placeTurn(playerName, target.id);
        }
    };

    const nextTurn = () => {
        setCurrPlayer(!currPlayer);
        setTurnNum(turnNum + 1);
    };

    const placeTurn = (player, position) => {
        const sideLength = Math.sqrt(props.boardSize);
        const row = Math.floor(position / sideLength);
        const col = position % sideLength;

        numBoard[row][col] = player;
        onTurnEnd(numBoard, player, row, col);
    };

    const checkWin = (board, player, row, col) => {
        const n = board.length;
        
        // Check the row
        let didWin = true;
        for (let j = 0; j < n; j++) {
            if (board[row][j] !== player) {
                didWin = false;
                break;
            }
        }
        if (didWin) return true;
        
        // Check the column
        didWin = true;
        for (let i = 0; i < n; i++) {
            if (board[i][col] !== player) {
                didWin = false;
                break;
            }
        }
        if (didWin) return true;
        
        // Check the main diagonal (top-left to bottom-right)
        if (row === col) {
            didWin = true;
            for (let i = 0; i < n; i++) {
                if (board[i][i] !== player) {
                    didWin = false;
                    break;
                }
            }
            if (didWin) return true;
        }
        
        // Check the anti-diagonal (top-right to bottom-left)
        if (row + col === n - 1) {
            didWin = true;
            for (let i = 0; i < n; i++) {
                if (board[i][n - 1 - i] !== player) {
                    didWin = false;
                    break;
                }
            }
            if (didWin) return true;
        }

        // No didWin found
        return false;
    };

    const onTurnEnd = (board, player, row, col) => {
        if (turnNum > props.boardSize / 2) {
            if (checkWin(board, player, row, col)) {
                setGameEndMessage(`${player} has won!!`);
            }
        }
        
        if (turnNum >= props.boardSize) {
            setGameEndMessage(`That's a draw ):`);
        }
        
        nextTurn();
    };

    return (
        <div className="board">
            {[...Array(props.boardSize).keys()].map(value => 
                <Cell id={value} key={value} imgAlt={DEFAULT_NAME} imgSrc={DEFAULT_IMG} handleClick={updateImage}></Cell>
            )}
            <h3>{gameEndMessage}</h3>
        </div>
    );
}

export default Board;