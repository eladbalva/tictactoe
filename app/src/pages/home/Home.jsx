import Header from "../../components/Header";
import Footer from "../../components/Footer";
import Card from "./components/Card";
import games from "./data.js"

function Home() {
    return (
        <div className="container">
            <Header></Header>
            <article>
                {games.map(game => <Card name={game.name} description={game.description} img={game.img} url={game.url}></Card>)}
            </article>
            <Footer></Footer>
        </div>
    );
}

export default Home;
