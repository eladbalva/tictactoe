import { Link } from "react-router-dom";

function Card({img, name = "", description = "", url}) {
    return (
        <article className="card">
            <img src={img} alt="game pic" className="card-pic"></img>
            <h2>{name}</h2>
            <p>{description}</p>
            <Link to={url}><button className="button">Play!</button></Link>
        </article>
    );
}

export default Card;