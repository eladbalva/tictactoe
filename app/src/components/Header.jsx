import { Outlet, Link } from "react-router-dom";

function Header() {
    return (
        <article>
            <h1>Best TicTacToe Ever</h1>
            <hr></hr>
            <nav className="navbar">
                    <p><Link to="/">Home</Link></p>
                    <p><Link to="https://en.wikipedia.org/wiki/HTTP_404">Login</Link></p>
            </nav>
            <Outlet />
        </article>
    );
}

export default Header;