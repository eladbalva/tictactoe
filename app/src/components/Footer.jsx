
function Footer() {
    return (
        <footer>
            <p>&copy; {new Date().getFullYear()} eladbalva productions</p>
        </footer>
    );
}

export default Footer;