import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./pages/home/Home.jsx";
import Game from "./pages/game/Game.jsx";

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/">
                    <Route index element={<Home />} />
                    <Route path="Game" index element={<Game />} />
                </Route>
            </Routes>
        </BrowserRouter>
    );
}

export default App;
